import cairo
import STK.FBSurface as FBSurface
import time
import STK.STKCore as STKCore
from evdev import ecodes

def cairoTriangle(ctx, x,y, scale):
	ctx.move_to(x, y)
	ctx.line_to(x, y+2*scale)
	ctx.line_to(x+2*scale, y+scale)
	ctx.line_to(x, y)
	ctx.fill()

def cairoFrame(ctx, x,y, width, height):
	ctx.set_source_rgb(1, 1, 1)
	ctx.set_line_width(1)
	ctx.move_to(x, y)
	ctx.line_to(x+width, y)
	ctx.line_to(x+width, y+height)
	ctx.line_to(x, y+height)
	ctx.line_to(x, y)
	ctx.stroke()
	
def cairoRect(ctx, x, y, width, height):
	ctx.set_source_rgb(1, 1, 1)
	ctx.rectangle(x, y, width, height)
	ctx.fill()

class Drawable():
	def __init__(self, width, height):
		self.updateRequest = STKCore.Signal([object])
		self._width = width
		self._height = height
	
	def width(self):
		return self._width
		
	def height(self):
		return self._height
		
	def resize(self, width, height):
		self._width = width
		self._height = height
		self.update()
	
	def update(self):
		self.updateRequest.emit(self)
		
	def _draw_(self, disp):
		pass

class Layout(Drawable):
	def __init__(self):
		Drawable.__init__(self, 0, 0)
		self.children = []
		self.parent = None
	
	def addWidget(self, widget):
		self.children.append(widget)
		widget.parent = self
		
		widget.updateRequest.connect(self.updateRequest.emit)
		
		#geometry calculation
		self._width = self._calcWidth_()
		self._height = self._calcHeight_()
		
	def removeWidget(self, widget):
		if widget in self.children:
			self.children.remove(widget)
			widget.parent = None
			widget.updateRequest.disconnect(self.updateRequest.emit)
		
		#geometry calculation
		self._width = self._calcWidth_()
		self._height = self._calcHeight_()
	
	def _draw_(self, ctx):
		for child in self.children:
			child._draw_(ctx)
			
	def handleKeyEvent(self, event):
		for child in self.children:
			child.handleKeyEvent(event)

class HBoxLayout(Layout):
	def _calcWidth_(self):
		width = 0
		for child in self.children:
			width = max(width, child.width())
		return width
		
	def _calcHeight_(self):
		height = 0
		for child in self.children:
			height += child.height()
		return height
	
	def getOffsets(self, widget):
		if self.parent != None:
			x0, y0 = self.parent.getOffsets(self)
		else:
			x0 = y0 = 0
		for child in self.children:
			if child == widget:
				return x0, y0
			y0 += child.height()

class VBoxLayout(Layout):
	def _calcWidth_(self):
		width = 0
		for child in self.children:
			width += child.width()
		return width
		
	def _calcHeight_(self):
		height = 0
		for child in self.children:
			height = max(height, child.height())
		return height
	
	def getOffsets(self, widget):
		if self.parent != None:
			x0, y0 = self.parent.getOffsets(self)
		else:
			x0 = y0 = 0
		for child in self.children:
			if child == widget:
				return x0, y0
			x0 += child.width()		
			
class Widget(Drawable):
	
	menuName = None
	
	def __init__(self, width, height):
		Drawable.__init__(self, width, height)
		self.parent = None
		self.rootLayout=None
		self.actions = []
		self.properties = []
		
	def setRootLayout(self, rootLayout):
		if self.rootLayout != None:
			self.rootLayout.disconnect(self.updateRequest.emit)
		self.rootLayout = rootLayout
		rootLayout.updateRequest.connect(self.updateRequest.emit)
	
	def handleKeyEvent(self, event):
		if self.rootLayout != None:
			self.rootLayout.handleKeyEvent(event)
	
	def _draw_(self, disp):
		if self.rootLayout != None:
			self.rootLayout._draw_(disp)

class FrameWidget(Widget):
	def _draw_(self, disp):
		x0, y0 = self.parent.getOffsets(self)
		cairoFrame(disp.ctx,x0,y0,self.width(),self.height())

class IconWidget(Widget):
	def __init__(self, width, height, filename):
		Widget.__init__(self, width, height)
		self.setIcon(filename)
		self.enabled = True
	
	def setIcon(self, filename):
		try:
			self.ims = cairo.ImageSurface.create_from_png(filename)
			self.imgctx = cairo.Context(self.ims)
			self.imgctx.scale(0.5, 0.5)
			self.filename = filename
		except:
			self.ims = None
			print("could not set icon %s"%filename)
		
	def _draw_(self, disp):
		if self.enabled and self.ims != None:
			x0, y0 = self.parent.getOffsets(self)
			disp.ctx.set_source_surface(self.ims, x0, y0)
			disp.ctx.paint()
		
	def enable(self):
		self.enabled = True

	def disable(self):
		self.enabled = False

	def toggle(self):
		if self.enabled:
			self.disable()
		else:
			self.enable()
		
class TextWidget(Widget):
	def __init__(self, width, height, xoffset=0, fontsize = 8, scrollDelay=1):
		Widget.__init__(self, width, height)
		self.xoffset = xoffset
		self.fontsize = fontsize
		self.calcGeometry()
		self.scrollNeeded = []
		self.scrollIndex = 0
		self.clearText()
		self.scrollTimer = STKCore.Timer(scrollDelay)
		self.scrollTimer.expired.connect(self._handleScrollEvent)
		STKCore.Application.timerMod.addTimer(self.scrollTimer)
		
	def _handleScrollEvent(self):
		if len(self.scrollNeeded) > 0:
			self.updateRequest.emit(self)

	def setText(self, text, i=0):
		self.textList[i] = text
		if len(text) > self.lettersPerLine:
			self.scrollNeeded.append(i)
		else:
			if i in self.scrollNeeded:
				self.scrollNeeded.remove(i)
	
	def calcGeometry(self):
		self.letterheight = self.fontsize
		self.letterwidth = int(2/3.*self.fontsize)
		self.lettersPerLine = int(self.width()//self.letterwidth)
		self.lines = (self.height()//self.fontsize)

	def resize(self, x, y):
		Widget.resize(self, x, y)
		self.calcGeometry()
				
	def clearText(self):
		self.textList = [""] * ( self.height() // self.fontsize )

	def _draw_(self, disp):
		x0, y0 = self.parent.getOffsets(self)
		disp.ctx.set_source_rgb(1, 1, 1)
		disp.ctx.select_font_face("Monospace", cairo.FONT_SLANT_NORMAL, 
				        cairo.FONT_WEIGHT_NORMAL)
		disp.ctx.set_font_size(self.fontsize)
		for i,lineNo in enumerate(range(self.fontsize,self.height()+1,self.fontsize)):
			disp.ctx.move_to(x0+self.xoffset, y0+lineNo-1)
			if i in self.scrollNeeded:
				disp.ctx.show_text(str(self.textList[i][self.scrollIndex:min(len(self.textList[i]), self.scrollIndex+self.lettersPerLine)]))
				self.scrollIndex += 1
				if self.scrollIndex+self.lettersPerLine > max([len(text) for text in self.textList]):
					self.scrollIndex = 0
			else:
				disp.ctx.show_text(str(self.textList[i]))

class MenuWidget(TextWidget):
	def __init__(self, width, height, rootmenu, fontsize = 10):
		TextWidget.__init__(self, width, height, xoffset = 10, fontsize=fontsize)
		self.currentMenu = rootmenu
		self.currentIndex = 0
		self.parentMenus = []
		self.parentIndices = []
		self.leave = STKCore.Signal([])

	def setText(self, textList, startIndex = 0):
		self.clearText()
		for i in range(0,min(len(self.textList), len(textList)-startIndex)):
			self.textList[i] = textList[i+startIndex]

	def drawCoursor(self, disp, line):
		x0, y0 = self.parent.getOffsets(self)
		cairoTriangle(disp.ctx, x0+2,y0+line*self.fontsize+2,3)

	def _draw_(self, disp):
		self.setText(self.currentMenu.keys(), max(self.currentIndex-self.lines+1,0))
		self.drawCoursor(disp, min(self.currentIndex,self.lines-1))
		TextWidget._draw_(self, disp)

	def handleKeyEvent(self, event):
		if event.value != 1: return
		if event.code == ecodes.KEY_UP:
			self.menuMoveUp()
			self.update()
		if event.code == ecodes.KEY_DOWN:
			self.menuMoveDown()
			self.update()
		if event.code in [ecodes.KEY_RIGHT, ecodes.KEY_ENTER, ecodes.KEY_OK]:
			self.submenuEnter()
			self.update()
		if event.code in [ecodes.KEY_LEFT, ecodes.KEY_BACK]:
			self.submenuLeave()
			self.update()

	def menuMoveDown(self):
		self.currentIndex = min(self.currentIndex+1, self.currentMenu.length()-1)
		
	def menuMoveUp(self):
		self.currentIndex = max(self.currentIndex-1, 0)
		
	def submenuEnter(self):
		if self.currentMenu._entries[self.currentIndex]['action'] != None:
			self.currentMenu._entries[self.currentIndex]['action'].do()
			
		if self.currentMenu._entries[self.currentIndex]['submenu'] != None:
			self.parentIndices.append(self.currentIndex)
			self.parentMenus.append(self.currentMenu)
			self.currentMenu = self.currentMenu._entries[self.currentIndex]['submenu']
			if isinstance(self.currentMenu, STKCore.PropertyMenu):
				self.currentMenu.rebuild()
			self.currentIndex = self.currentMenu.defaultEntry
		
	def submenuLeave(self):
		if len(self.parentMenus) <= 0: 
			self.leave.emit()
			return
		self.currentMenu = self.parentMenus.pop()
		self.currentIndex = self.parentIndices.pop()
			
class Display:
	def __init__(self, surface):
		self.surf = surface
		self.width, self.height = self.surf.get_width(), self.surf.get_height()
		self.ctx = cairo.Context(self.surf)
		self.widget = None
		self.clear()

	def clear(self):
		self.ctx.set_operator(cairo.OPERATOR_CLEAR)
		self.ctx.set_source_rgba(0, 0, 0, 1)
		self.ctx.paint()
		self.ctx.set_operator(cairo.OPERATOR_OVER)
		
	def setWidget(self, widget):
		if self.widget != None:
			self.widget.updateRequest.disconnect(self._handleDrawRequest_)
		widget.updateRequest.connect(self._handleDrawRequest_)
		self.widget = widget
		widget.update()

	def _handleDrawRequest_(self, drawable):
		self.clear()
		#drawable._draw_(self)
		self.draw()

	def handleKeyEvent(self, event):
		self.widget.handleKeyEvent(event)

	def draw(self):
		if self.widget != None:
			self.widget._draw_(self)
		
	def update(self):
		self.clear()
		self.draw()
		
	def close(self):
		FBSurface.close_fbmem(self.fbmem)
