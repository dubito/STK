from subprocess import call
import time
from evdev import InputDevice,ecodes

def MenuForProperty(prop):
	newMenu = Menu()
	for value in prop.choices():
		newMenu.addAction(FunctionAction(value, prop.cbset, value))
	newMenu.defaultEntry = prop.defaultIdx()
	return newMenu

def ActionMenu(actionList):
	return [ {'name':"%s"%action.name, 'action': action, 'submenu':None} for action in actionList]

class Menu:
	def __init__(self):
		self._entries = []
		self.defaultEntry = 0
		
	def addMenu(self, menu, name):
		self._entries.append({'name':name, 'action':None, 'submenu':menu})
		
	def addAction(self, action):
		self._entries.append({'name':action.name, 'action':action, 'submenu':None})
		
	def getEntries(self):
		return self._entries
		
	def keys(self):
		return [entry['name'] for entry in self._entries]
		
	def length(self):
		return len(self._entries)
		
class PropertyMenu(Menu):
	def __init__(self, prop):
		Menu.__init__(self)
		self.prop = prop
		self.rebuild()
		
	def rebuild(self):
		self._entries = []
		for value in self.prop.choices():
			self.addAction(FunctionAction(value, self.prop.cbset, value))
		if self.prop.defaultIdx() != None:
			self.defaultEntry = self.prop.defaultIdx()
		else:
			self.defaultEntry = 0

class ModuleProperty:
	def __init__(self, name, cbset=None, cbget=None, cbdefault=None):
		self.name = name
		self.cbset = cbset
		self.cbget = cbget
		self.cbdefault = cbdefault
		
	def choices(self):
		if self.cbget != None:
			return self.cbget()
		else:
			return []
			
	def default(self):
		if self.cbdefault != None:
			return self.cbdefault()
			
	def defaultIdx(self):
		choices = self.choices()
		default = self.default()
		if default in choices:
			return choices.index(default)
			
	def setValue(self, val):
		if self.cbset != None:
			return self.cbset(val)
			
	def setIdx(self, idx):
		#be careful with this function as choices is generated dynamically
		#it could have changend from the time when the idx was calculated
		#to now!
		choices = self.choices()
		if idx < len(choices):
			self.setValue(choices[idx])

class Action:
	def __init__(self, name):
		self.name = name
	
	def do(self):
		raise NotImplementedError("Implement action")
		
class RunCmdAction(Action):
	def __init__(self, name, cmd):
		Action.__init__(self, name)
		self.cmd = cmd
	
	def do(self):
		print(self.cmd)
		return call((self.cmd).split(' '))
		
class SettingsAction(Action):
	def __init__(self, name, settingsKey, value):
		Action.__init__(self, name)
		self.settingsKey = settingsKey
		self.value = value
		
	def do(self):
		 Application.settings[self.settingsKey] = self.value
		 
class FunctionAction(Action):
	def __init__(self, name, func, *args):
		Action.__init__(self, name)
		self.func = func
		self.args = args
		
	def do(self):
		try:
			self.func(*self.args)
		except Exception as e:
			if self.func != None:
				print("could not run %s: %s"%(self.func.__name__, e))
			else:
				print(e)
		
class Signal:
	def __init__(self, paraList):
		self.consumerList = []
		self.paraList = paraList
		
	def emit(self, *args):
		assert len(args) == len(self.paraList)
		for consumer in self.consumerList:
			consumer(*args)
			
	def connect(self, consumer):
		self.consumerList.append(consumer)
		
	def disconnect(self, consumer):
		if consumer in self.consumerList:
			self.consumerList.remove(consumer)
			
class MainloopModule:
	def _dojob_(self):
		pass

class InputModule(MainloopModule):
	
	def __init__(self, evdev):
		self.evdev = InputDevice(evdev)
		self.keyEvent = Signal([object])
		
	def _dojob_(self):
		event = self.evdev.read_one()
		if event == None: return
		if event.type == ecodes.EV_KEY:
			self.keyEvent.emit(event)
				
class Timer:
	
	def __init__(self, delay):
		self. delay = delay
		self.expired = Signal([])
		self.lastFired = 0
		
	def fire(self):
		self.expired.emit()
		self.lastFired = time.time()
		
class SynchronizedTimer(Timer):
	def __init__(self, syncedDelay):
		Timer.__init__(self, syncedDelay)
		self.syncedDelay = syncedDelay
	
	def fire(self):
		self.delay = self.syncedDelay-(time.time()%self.syncedDelay)
		Timer.fire(self)
		
		
class TimerModule(MainloopModule):
	
	def __init__(self):
		self.timerList = []
		
	def _dojob_(self):
		current = time.time()
		for timer in self.timerList:
			if timer.lastFired+timer.delay <= current:
				timer.fire()
	
	def addTimer(self, timer):
		self.timerList.append(timer)
		timer.fire()
		
	def getTimer(self, delay):
		timer = Timer(delay)
		self.addTimer(timer)
		return timer
		
class Application:
	
	timerMod = TimerModule()
	settings = {}
	
	def __init__(self, defaultSettings={}):
		self.settings.update(defaultSettings)
		self.keepRunning = True
		self.moduleList = [self.timerMod]
	
	def addModule(self, module):
		self.moduleList.append(module)
		
	def start(self):
		while self.keepRunning:
			for module in self.moduleList:
				module._dojob_()
			time.sleep(1e-2)

