STK is a cairo based simple toolkit for low resolution monochrome 
framebuffers.

Most available GUI toolkits are developed with high resolution color 
displays in mind. Therefore they are often unsuited for low resolution 
monochrome displays like small LCD or OLED modules. STK tries to fill 
this gap and offer a toolkit for basic graphical elements for low 
resolution displays.

The API is loosely based on the Qt API.

STK depends on pycairo and Python-evdev
